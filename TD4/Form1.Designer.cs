﻿namespace TD4
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_calcul = new System.Windows.Forms.Button();
            this.tb_valSaisie = new System.Windows.Forms.TextBox();
            this.lbl_resultat = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rad_square = new System.Windows.Forms.RadioButton();
            this.rad_cube = new System.Windows.Forms.RadioButton();
            this.btn_quitter = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_calcul
            // 
            this.btn_calcul.Location = new System.Drawing.Point(335, 209);
            this.btn_calcul.Name = "btn_calcul";
            this.btn_calcul.Size = new System.Drawing.Size(75, 23);
            this.btn_calcul.TabIndex = 0;
            this.btn_calcul.Text = "Calcul";
            this.btn_calcul.UseVisualStyleBackColor = true;
            this.btn_calcul.Click += new System.EventHandler(this.btn_calcul_Click);
            // 
            // tb_valSaisie
            // 
            this.tb_valSaisie.Location = new System.Drawing.Point(49, 124);
            this.tb_valSaisie.Name = "tb_valSaisie";
            this.tb_valSaisie.Size = new System.Drawing.Size(100, 22);
            this.tb_valSaisie.TabIndex = 2;
            // 
            // lbl_resultat
            // 
            this.lbl_resultat.AutoSize = true;
            this.lbl_resultat.Location = new System.Drawing.Point(272, 124);
            this.lbl_resultat.Name = "lbl_resultat";
            this.lbl_resultat.Size = new System.Drawing.Size(46, 17);
            this.lbl_resultat.TabIndex = 3;
            this.lbl_resultat.Text = "label1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rad_cube);
            this.groupBox1.Controls.Add(this.rad_square);
            this.groupBox1.Location = new System.Drawing.Point(49, 189);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Calcul";
            // 
            // rad_square
            // 
            this.rad_square.AutoSize = true;
            this.rad_square.Checked = true;
            this.rad_square.Location = new System.Drawing.Point(7, 22);
            this.rad_square.Name = "rad_square";
            this.rad_square.Size = new System.Drawing.Size(64, 21);
            this.rad_square.TabIndex = 0;
            this.rad_square.TabStop = true;
            this.rad_square.Text = "Carré";
            this.rad_square.UseVisualStyleBackColor = true;
            // 
            // rad_cube
            // 
            this.rad_cube.AutoSize = true;
            this.rad_cube.Location = new System.Drawing.Point(7, 50);
            this.rad_cube.Name = "rad_cube";
            this.rad_cube.Size = new System.Drawing.Size(62, 21);
            this.rad_cube.TabIndex = 1;
            this.rad_cube.Text = "Cube";
            this.rad_cube.UseVisualStyleBackColor = true;
            // 
            // btn_quitter
            // 
            this.btn_quitter.Location = new System.Drawing.Point(335, 239);
            this.btn_quitter.Name = "btn_quitter";
            this.btn_quitter.Size = new System.Drawing.Size(75, 23);
            this.btn_quitter.TabIndex = 5;
            this.btn_quitter.Text = "Quitter";
            this.btn_quitter.UseVisualStyleBackColor = true;
            this.btn_quitter.Click += new System.EventHandler(this.btn_quitter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_quitter);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbl_resultat);
            this.Controls.Add(this.tb_valSaisie);
            this.Controls.Add(this.btn_calcul);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_calcul;
        private System.Windows.Forms.TextBox tb_valSaisie;
        private System.Windows.Forms.Label lbl_resultat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rad_cube;
        private System.Windows.Forms.RadioButton rad_square;
        private System.Windows.Forms.Button btn_quitter;
    }
}

