﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TD4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calcul_Click(object sender, EventArgs e)
        {
            if (tb_valSaisie.Text == "")
            {
                lbl_resultat.Text = "Erreur";
            }
            else
            {
                double val;
                try
                {
                    val = double.Parse(tb_valSaisie.Text);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message);
                    return;
                }

                double res = 0;
                if (rad_square.Checked == true)
                    res = Math.Pow(val, 2);
                else if (rad_cube.Checked == true)
                    res = Math.Pow(val,3);
                lbl_resultat.Text = res.ToString();
            }
            }
        
        private void btn_quitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        } 
            
    }
}
